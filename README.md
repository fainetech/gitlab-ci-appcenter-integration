# gitlab-ci-appcenter-integration

This is a simple GitLab CI job for queueing build on [AppCenter.ms](https://appcenter.ms) 🚀 and representing the status of the build GItLabCI pipelines. Inspired by [Gerade Geldenhuys post](https://dev.to/raidzen10/integrate-your-ci-cd-with-appcenter-7ae) 🚀❤️ where he automated this process with powershell and approach how [GitVersion.Tool](https://gitlab.com/guided-explorations/devops-patterns/utterly-automated-versioning/-/tree/develop) created integration for GitLab CI pipelines.

## How to use

* [appcenter-ci-cd-plugin.gitlab-ci.yml](appcenter-ci-cd-plugin.gitlab-ci.yml) - templated job to queue build on AppCenter
* [.gitlab-ci.yml](.gitlab-ci.yml) - sample how to use integration.

```yaml

include:
  - local: 'appcenter-ci-cd-plugin.gitlab-ci.yml'

build-android:
  extends: .appcenter-build
  stage: sample
  variables:
    # AppCenter User token
    MOBILE_CENTER_TOKEN: ${YOUR_USER_TOKEN}
    # Sets application context for appcenter
    MOBILE_CENTER_CURRENT_APP: '<OWNER>/<YOUR_APP>'

```

## Configuration

|ENV Variable|Description|
|---|---|
|MOBILE_CENTER_TOKEN|Required. User Token with Full Access ([docs](https://docs.microsoft.com/en-us/appcenter/api-docs/#section02))|
|MOBILE_CENTER_CURRENT_APP|Required. AppCenter app identifier. Can be obtained by appcenter-cli `appcenter apps list` or from application page URL: `https://appcenter.ms/orgs/<OWNER>/apps/<YOUR_APP>`|
|MOBILE_CENTER_CURRENT_BRANCH|Defaults to ${CI_COMMIT_REF_NAME}. **Important** Branch must be manually configured on AppCenter ([docs](https://docs.microsoft.com/uk-ua/appcenter/build/branches))|
